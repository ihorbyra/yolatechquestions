# Yola Tech Questions

## Instructions

This file is a markdown document which has some basic technical questions to help assess your fit within the Engineering Team at Yola. Please provide your solutions to the problems within this markdown file.

Once completed, email your solutions with the filename `tech-solutions-{{first_name}}-{{last_name}}.md`

## Engineering

**Do you lint your JS and CSS? If yes, what tools do you use?**

**When writing your CSS, do you use classes or IDs? And why?**

**Help a coworker**

Assume your coworker is trying to understand:
```
console.log('one');
setTimeout(function() {
  console.log('two');
}, 0);

Promise.resolve('four').then(console.log)
console.log('three');
```

The coworker is confused and thinks the output should be:
```
one
two
three
four
```

Give an explanation that should clear up any confusion.

**What is DOM?**

**What is a promise?**

**What is closure?**

**When is it appropriate to use jQuery and when is it not?**

**Fix this code**

The cow and the pig should eat at the same time, and only after that you should eatLunch. As-is cow feeding blocks pig feeding. Assume `feedCow` and `feedPig` return immediately with `undefined`
and will call the passed-in callback when finished.

```
function main(cb) {
  var cow = new Cow();
  var pig = new Pig();
  feedCow(cow, function() {
    feedPig(pig, function() {
      eatLunch(function() {
        cb();
      });
    });
  });
}
```

**Write some code to render this piece of data as html (without libraries/frameworks usage).**

```
{
  title: "Farm Animals",
  results: [
    { name: "pig", icon: "/static/pig.221f.jpg", description: "Goes 'oink!'" },
    { name: "cow", icon: "/static/cow.1dd9.jpg", description: "Says 'moo'." },
    { name: "dog", icon: "/static/dog.9a82.jpg", description: "Barks 'woof!'" },
    { name: "cat", icon: "/static/cat.c07e.jpg", description: "Meow." },
  ]
}
```

## General

**Do you have any experience in working with foreign colleagues?**

**What skill do you feel is missing from your resume?**

**What's your experience with code review?**

**What is the last thing you learned?**

**What would be an ideal work assignment that would utilize your best skills?**